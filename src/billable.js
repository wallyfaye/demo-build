import fs from 'fs';
import parse from 'csv-parse';
import get_files_by_type from './get_files_by_type/get_files_by_type'

export default class billable {

	constructor(params){
		
		this.invoice_dir = params.invoice_dir;
		this.invoice_filenames = [];
		this.invoice_data = [];

	}

	async get_billable(callback) {

		return new Promise( (resolve) => {

			this.get_invoice_list(async response => {

				this.invoice_filenames = response.invoices;

				const parsed_promises = this.invoice_filenames.map(async file_name => {
					return await this.parse_invoice(this.invoice_dir + '/' + file_name);
				});

				for (const parsed_promise of parsed_promises){
					this.invoice_data = this.invoice_data.concat(await parsed_promise);
				}

				resolve();

			});

		});

	}

	get_invoice_list(on_list_pulled) {

		get_files_by_type({
			dir: this.invoice_dir,
			file_type: 'csv'
		}).then(on_list_pulled);

	}

	parse_invoice(file_path) {

		return new Promise((resolve, reject) => {

			const parser = parse({
				delimiter: ',',
				columns: true
			}, (err, data) => {
				resolve(data);
			});

			fs.createReadStream(file_path).pipe(parser);

		});

	}

}