import fs from 'fs';
import path from 'path';

export default async function get_files_by_type (params) {

	const dir = params.dir;
	const file_type = params.file_type;

	try{

		return await {
			invoices: fs.readdirSync(dir).filter(file => path.extname(file) == '.' + file_type)
		};

	} catch (err) {

		return {
			invoices: [],
			error: err
		};
		
	}


}