import get_files_by_type from './../src/get_files_by_type/get_files_by_type'

import { assert } from 'chai';

describe('get_files_by_type', function() {

	it('should provide an array of csv files from a valid directory', function() {

		get_files_by_type({
			dir: './test/invoices',
			file_type: 'csv'
		}).then(function(response){

			assert.isAbove(response.invoices.length, 0);
			assert.isUndefined(response.error);

		});

	});

	it('should have no invoices and include an error when a bad directory is specified', function() {

		get_files_by_type({
			dir: 'some_bad_dir',
			file_type: 'csv'
		}).then(function(response){

			assert.equal(response.invoices.length, 0);
			assert.isDefined(response.error);

		});

	});

});